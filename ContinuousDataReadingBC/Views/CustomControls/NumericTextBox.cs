﻿using System.Windows.Controls;
using System.Windows.Input;

namespace ContinuousDataReadingBC.Views.CustomControls
{
    public class NumericTextBox : TextBox
    {
        protected override void OnPreviewTextInput(TextCompositionEventArgs e)
        {
            e.Handled = !double.TryParse(e.Text, out _);
        }
    }
}
