﻿using ContinuousDataReadingModel.Models;
using ContinuousDataReadingModel.Services;
using ContinuousDataReadingModel.Services.Interfaces;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ContinuousDataReadingBC.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region Properties

        public string TestText { get; set; }
        public int RefreshTime { get; set; }

        public bool IsRunning { get; set; }

        public SeriesCollection ChartSeries { get; set; }
        public ChartValues<DateTimePoint> ChartValues { get; set; }
        public Func<double, string> YFormatter { get; set; }
        public Func<double, string> XFormatter { get; set; }

        public ObservableCollection<Weather> WeatherCollection { get; set; }

        #endregion

        #region Fields

        private CancellationTokenSource _cancellationTokenSource;
        private readonly IWeatherService _weatherService;

        #endregion

        #region Constructor

        public MainWindow()
        {
            InitializeComponent();

            _weatherService = new WeatherService();
            WeatherCollection = new ObservableCollection<Weather>();
            RefreshTime = 2;
            XFormatter = val => new DateTime((long)val).ToString("HH:mm:ss");
            YFormatter = val => val.ToString("N") + " °C";
            InitChartSeries();

            DataContext = this;
        }

        #endregion

        #region UI Events

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            _cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = _cancellationTokenSource.Token;

            Task.Run(() => RunServer(cancellationToken), _cancellationTokenSource.Token);
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            _cancellationTokenSource.Cancel();
        }

        #endregion

        #region Private Methods

        private void RunServer(CancellationToken cancellationToken)
        {
            IsRunning = true;
            while (true)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    IsRunning = false;
                    break;
                }

                Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(async () =>
                {
                    var weather = await _weatherService.GetWeatherAsync();
                    _weatherService.ProcessWeather(weather);
                    WeatherCollection.Add(weather);
                    AddPointToChart(weather);
                }));

                cancellationToken.WaitHandle.WaitOne(RefreshTime * 1000);
            }
        }

        private void AddPointToChart(Weather weather)
        {
            ChartValues.Add(new DateTimePoint(weather.Time, weather.Temp));
            ChartValues.RemoveAt(0);
        }

        private void InitChartSeries()
        {
            ChartSeries = new SeriesCollection();
            ChartValues = new ChartValues<DateTimePoint>();

            for (int i = 20; i >= 0; i--)
            {
                ChartValues.Add(new DateTimePoint(DateTime.Now.AddSeconds(-RefreshTime * i), 0));
            }

            ChartSeries.Add(new LineSeries
            {
                Values = ChartValues,
                LineSmoothness = 0
            });
        }

        #endregion

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged = (sender, e) => { };

        public void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}
