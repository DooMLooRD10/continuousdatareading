﻿using System;

namespace ContinuousDataReadingModel.Models
{
    public class Weather
    {
        public double Temp { get; set; }
        public double Pressure { get; set; }
        public double Humidity { get; set; }
        public double Temp_min { get; set; }
        public double Temp_max { get; set; }
        public DateTime Time { get; set; }
    }
}
