﻿using ContinuousDataReadingModel.Models;
using System.Threading.Tasks;

namespace ContinuousDataReadingModel.Services.Interfaces
{
    public interface IWeatherService
    {
        Task<Weather> GetWeatherAsync();
        void ProcessWeather(Weather weather);
    }
}
